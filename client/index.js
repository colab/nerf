//document.addEventListener('DOMContentReady', function() {
var base = ''
var bindings = [
    {
        'buttonSelector': '#up',
        'key': 38,
        'command': 'up'
    },
    {
        'buttonSelector': '#down',
        'key': 40,
        'command': 'down'
    },
    {
        'buttonSelector': '#left',
        'key': 37,
        'command': 'left'
    },
    {
        'buttonSelector': '#right',
        'key': 39,
        'command': 'right'
    },
    {
        'buttonSelector': '#fire',
        'key': 32,
        'command': 'fire'
    },
]

bindControls(bindings);

function bindControls(bindings) {

    for (var i = 0; i < bindings.length; i++) {
        console.log('adding event listener for button: ' + bindings[i].buttonSelector + ' -> ' + bindings[i].command);
        document.querySelector(bindings[i].buttonSelector).addEventListener('click', sendCommand.bind(null, bindings[i].command));
    }

    document.addEventListener('keydown', function(bindings, ev) {
        var key = ev.keyCode;
        for (var i = 0; i < bindings.length; i++) {
            if (key === bindings[i].key) {
                sendCommand(bindings[i].command);
                highlight(document.querySelector(bindings[i].buttonSelector));
            }
        }
    }.bind(null, bindings));
}

function highlight(el) {
    el.style.backgroundColor = 'rgb(30,72,110)';
    setTimeout(function(el) {
        el.style.backgroundColor = '';
    }.bind(null,el),150);
}

function sendCommand(command) {
    var x = new XMLHttpRequest();
    var url = 'http://nerf.colab.duke.edu:8080/command?command=' + encodeURIComponent(command);
    console.log('sending ' + url);
    x.open('GET', url);
    x.send();
}

//});