///<reference path="typings/main.d.ts" />
var http = require('http');
var https = require('https');
var url = require('url');
var path = require('path');
var fs = require('fs');

var TOKEN = '354036a2fc6dc05b7e91170169e32634ffa9f87f';

http.createServer(function(req, res) {
    try {
        res.setHeader('Access-Control-Allow-Origin', '*');


        var requestData = url.parse(req.url, true);

        var command = requestData.query.command;

        console.log('dynamic: Sending command ' + command);

        var req = https.request({
            'method': 'post',
            'host': 'api.particle.io',
            'headers': {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            'path': '/v1/devices/2a0036000347343339373536/move'
        }, function(res) {
            res.setEncoding('utf8');
            res.on('data', function(chunk) {
                console.log('Response: ' + chunk);
            });
        });
        req.write('arg=' + command + '&' + 'access_token=' + TOKEN);
        req.end();
        res.end();
    } catch (e) {
        res.end();
    }
}).listen(8080);
console.log('Started 8080 server');

http.createServer(function(req, res) {

    var url = req.url;
    url.replace('..', '');
    url.replace('~', '');
    url = url.substr(1);
    if (url === '' || url === '/') {url = 'index.html';} 
    console.log('static: Requesting ' + url + '(' + path.join(__dirname, '../static', req.url) + ')');
    var stream = fs.createReadStream(path.join(__dirname, '../client', req.url));
    stream.on('error', function(err) {
        console.log(err);
        res.writeHead(404);
        res.end();
    });
    stream.pipe(res);
}).listen(80);

console.log('Started 80 server');